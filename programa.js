class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.709143568562426,-74.22394931316376];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion)
    {
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono (posicion)
    {
        this.poligonos.push(L.polygon(posicion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.709143568562426,-74.22394931316376]);
miMapa.colocarMarcador([4.707871145772687, -74.22340750694275]);



miMapa.colocarCirculo([4.709143568562426,-74.22394931316376], {
    color: 'blue',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
});
miMapa.colocarCirculo([4.709143568562426,-74.22394931316376], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 50
});


miMapa.colocarPoligono([
    [4.710923887298141,-74.22375082969666],
    [4.709389498749167,-74.2258483171463],
    [4.7072990893903865,-74.22461450099945],
    [4.70771610250059, -74.22418534755707],
    [4.708202617479911, -74.22416388988495],
    [4.70849131851558,-74.22400295734406],
    [4.708614283735123,-74.22382056713104],
    [4.708747941557802,-74.22338604927063],
    [4.70864101530172,-74.22304272651672],
    [4.7089190235333565, -74.22268867492674],
    [4.710923887298141, -74.22375082969666]
    
])


